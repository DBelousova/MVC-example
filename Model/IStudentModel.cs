﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;

namespace Model
{
    public interface IStudentModel
    {
        IEnumerable<Student> Students { get; set; }
        event EventHandler<StudentEventArgs> StudentUpdated;
        void UpdateStudent(Student student);

    }
}
