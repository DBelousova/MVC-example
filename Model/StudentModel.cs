﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;

namespace Model
{
    public class StudentModel : IStudentModel
    {
        public IEnumerable<Student> Students { get; set; }
        public event EventHandler<StudentEventArgs> StudentUpdated;
        public void UpdateStudent(Student student)
        {
            //сделать по аналогии два метода: добавить и удалить, посмотрите как реализовано обновление в примере для заочников
            student.Name = "mickey mouse";
            StudentUpdated?.Invoke(this, new StudentEventArgs(student));
        }

        public StudentModel()
        {
            Students = new StudentService().GetStudents();
        }

    }
}
