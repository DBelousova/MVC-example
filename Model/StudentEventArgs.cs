﻿using DataAccess;

namespace Model
{
    public class StudentEventArgs
    {
        public Student Student { get; set; }

        public StudentEventArgs(Student student)
        {
            Student = student;
        }
    }
}