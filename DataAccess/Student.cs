﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class Student
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public void Update(Student student)
        {
            Name = student.Name;
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
}
