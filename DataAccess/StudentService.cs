﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class StudentService : IStudentService
    {
        public IList<Student> GetStudents()
        {
            List<Student> students = new List<Student>()
            {
                new Student()
                {
                    ID = 0,
                    Name = "Tom"
                },
                new Student()
                {
                    ID = 1,
                    Name = "Jerry"
                }
            };

            return students;
        }
    }
}
