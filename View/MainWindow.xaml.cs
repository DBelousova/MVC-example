﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DataAccess;
using Model;
using View.Controller;

namespace View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IStudentController studentController;
        private Student selectedStudent;
        

        public MainWindow()
        {
            InitializeComponent();
            var model = new StudentModel();
            model.StudentUpdated += ModelOnStudentUpdated;
            studentController = new StudentController(model);
            Studens.ItemsSource = studentController.GetStudents();
        }

        private void ModelOnStudentUpdated(object sender, StudentEventArgs studentEventArgs)
        {
            var a = studentController.GetStudents();
            Studens.ItemsSource = studentController.GetStudents();
        }


        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            if (Studens.SelectedValue != null)
            {
                studentController.Update((Student)Studens.SelectedValue);
            }
        }
    }
}
