﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;

namespace View.Controller
{
    public interface IStudentController
    {
        void Update(Student student);

        IEnumerable<Student> GetStudents();
    }
}
