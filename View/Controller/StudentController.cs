﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using Model;

namespace View.Controller
{
    public class StudentController : IStudentController
    {
        private readonly IStudentModel _model;

        public StudentController(IStudentModel studentModel)
        {
            _model = studentModel;
        }


        public void Update(Student student)
        {
            //реализуйте удаление, добавление
            _model.UpdateStudent(student);
        }

        public IEnumerable<Student> GetStudents()
        {
            return _model.Students.ToList();
        }
    }
}
